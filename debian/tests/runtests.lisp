(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "anaphora/test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (rt:do-tests)
  (uiop:quit 1))
